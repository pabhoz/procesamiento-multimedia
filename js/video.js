//VIDEO PROCESSING
var processor = {
  timerCallback: function() {
    if (this.video.paused || this.video.ended) {
      return;
    }
    this.computeFrame();
    var self = this;
    setTimeout(function () {
        self.timerCallback();
      }, 0);
  },
  setFilter: function(filter){
    this.filter = filter;
  },
  getFilteredFrame: function(frame){
    switch (this.filter){
      case "byn":
      return Filters.grayscale(frame);
      break;
      case "brillo":
      return Filters.brightness(frame,50);
      break;
      case "sharpen":
      return Filters.convolute(frame,[ 0, -1,  0,
           -1,  5, -1,
            0, -1,  0],false);
      break;
      case "invert":
      return Filters.invert(frame);
      break;
      default:
        return frame;
      break;
    }
  },
  doLoad: function() {

    var video = document.createElement("video");
    video.src = "./vids/sao-op1.webm";
    video.width = 600;
    video.height = 337;
    video.autoplay = true;
    document.getElementById("oVideo").appendChild(video);

    this.video = video;

    this.c1 = document.createElement("canvas");
    this.c1.width = this.video.width;
    this.c1.height = this.video.height;
    this.ctx1 = this.c1.getContext("2d");

    this.c2 = document.createElement("canvas");
    this.c2.width = this.video.width;
    this.c2.height = this.video.height;
    this.ctx2 = this.c2.getContext("2d");
    document.getElementById("pVideo").appendChild(this.c2);

    var self = this;

    this.video.addEventListener("play", function() {
        self.width = self.video.width;
        self.height = self.video.height;
        self.timerCallback();
      }, false);
  },

  computeFrame: function() {
    this.ctx1.drawImage(this.video, 0, 0, this.width, this.height);
    var frame = this.ctx1.getImageData(0, 0, this.width, this.height);

    var processedFrame = this.getFilteredFrame(frame);
    this.ctx2.putImageData(processedFrame, 0, 0);

    return;
  }
};