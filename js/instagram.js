var image = new Image();

image.src = "./pics/kirito.jpg";
image.width = 320;
image.height = 200;

image.onload = function(){

	canvas = document.querySelector("#dropArea canvas");
    ctx = canvas.getContext("2d");
    ctx.drawImage(this,0,0,this.width, this.height);

	var canvas = document.getElementsByTagName("canvas")[0];

	var pixels = Filters.getPixels(this);

	var bynP = Filters.grayscale(pixels);
	addCanvas(bynP,"Blanco y Negro","byn");

	var brightnessP = Filters.brightness(pixels,100);
	addCanvas(brightnessP,"Brillo","bri");

	var pixels = Filters.getPixels(this);
	
	var chromaP = Filters.chroma(pixels,{r:156,g:119,b:123},10);
	addCanvas(chromaP,"Chroma","chr");

	var pixels = Filters.getPixels(this);
	
	var invertP = Filters.invert(pixels,{r:156,g:119,b:123},10);
	addCanvas(invertP,"Inversiòn","inv");

	$("#filtros .filtro").click(function(){
		changeFilter($(this)[0].filter);
	});

}

function addCanvas(imageData,titulo,filterName){

	var resultCanvas = document.createElement("canvas");
	resultCanvas.width = 80;
	resultCanvas.height = 80;

	var ctx = resultCanvas.getContext('2d');
	ctx.putImageData(imageData,0,0);

	var div = document.createElement("div");
	div.className = "filtro";
	var title = document.createElement("div");
	title.className = "title";

	title.innerHTML = titulo;

	div.appendChild(resultCanvas);
	div.appendChild(title);
	div.filter = filterName;
	
	document.body.querySelector("#filtros").appendChild(div);

}


	function changeFilter(filter){

		var canvas = document.getElementsByTagName("canvas")[0];

	  	var ctx = canvas.getContext('2d');
	  	ctx.drawImage(image,0,0,image.width, image.height);
	  	var pixels = ctx.getImageData(0,0,canvas.width,canvas.height);

	  	switch(filter){
	  		case "byn":
	  			var pixelsR = Filters.grayscale(pixels);
	  		break;
	  		case "bri":
	  			var pixelsR = Filters.brightness(pixels,100);
	  		break;
	  		case "chr":
	  			var pixelsR = Filters.chroma(pixels,{r:156,g:119,b:123},10);
	  		break;
	  		case "inv":
	  			var pixelsR = Filters.invert(pixels);
	  		break;
	  	}
		
		
		var ctx = canvas.getContext("2d");
		ctx.putImageData(pixelsR,0,0);

	}



