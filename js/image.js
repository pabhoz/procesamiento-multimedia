var image = new Image();

image.src = "./pics/kirito.jpg";
image.width = 320;
image.height = 200;

image.onload = function(){

	var pixels = Filters.getPixels(this);
	console.log(pixels);

	var c = Filters.getCanvas(this.width,this.height);
	var ctx = c.getContext('2d');
	ctx.drawImage(this,0,0,this.width, this.height);

	var div = document.createElement("div");
	var p = document.createElement("p");

	p.innerHTML = "Original";

	div.appendChild(p);
	div.appendChild(c);

	document.body.appendChild(div);

	var canvas = document.getElementsByTagName("canvas")[0];

	var bynP = Filters.grayscale(pixels);
	addCanvas(bynP,canvas,"Blanco y Negro");

	var pixels = Filters.getPixels(this);

	var brightnessP = Filters.brightness(pixels,100);
	addCanvas(brightnessP,canvas,"Brillo");

	var pixels = Filters.getPixels(this);
	
	var chromaP = Filters.chroma(pixels,{r:156,g:119,b:123},10);
	addCanvas(chromaP,canvas,"Chroma");

	var pixels = Filters.getPixels(this);
	
	var invertP = Filters.invert(pixels,{r:156,g:119,b:123},10);
	addCanvas(invertP,canvas,"Inversiòn");

	var pixels = Filters.getPixels(this);

	var sharpenP = Filters.convolute(pixels,
		[0,-1,0
		,-1,5,-1
		,0,-1,0], false);
	addCanvas(sharpenP,canvas,"Sharpen");

	var pixels = Filters.getPixels(this);

	var sharpenP = Filters.convolute(pixels,
		[-1,-1,0
		,-1,0,1
		,0,1,1], false);
	addCanvas(sharpenP,canvas,"Sharpen");

}

function addCanvas(imageData,refCanvas,title){

	var resultCanvas = document.createElement("canvas");
	resultCanvas.width = refCanvas.width;
	resultCanvas.height = refCanvas.height;

	var ctx = resultCanvas.getContext('2d');
	ctx.putImageData(imageData,0,0);

	var div = document.createElement("div");
	var p = document.createElement("p");

	p.innerHTML = title;

	div.appendChild(p);
	div.appendChild(resultCanvas);

	document.body.appendChild(div);

}